from flask import Flask, render_template, request, redirect, url_for

app = Flask(__name__)


@app.route('/')
def index():
    return redirect(url_for("hello"))

@app.route('/hello', methods=['GET', 'POST'])
def hello():
    points = []
    if request.method=='POST':
        x1 = 0
        x2 = 1000
        y1 = 0
        y2 = 2000
        if request.form.get('x1'):
            x1 = int(request.form['x1'])
        if request.form.get('y1'):
            y1 = int(request.form['y1'])
        if request.form.get('x2'):
            x2 = int(request.form['x2'])
        if request.form.get('y2'):
            y2 = int(request.form['y2'])

        x = float(x1)
        y = float(y1)
        try:
            m = float(x2 - x1) / float(y2 - y1)
        except ZeroDivisionError:
            m = 1.0
        points = []
        for i in range(x2 - x1):
            y += m
            x += 1 / m
            points.append([int(x), int(y)])

    return render_template("main.html", points=points)


if __name__ == '__main__':
    app.run(debug=True)
